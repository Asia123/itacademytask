import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Program {

    private static Map<String,Integer> users = new HashMap<>();


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int choice = 0;
        String path="users.csv";
        UserRegistry userRegistry = new UserRegistry();
        String name;
        int age;

        do {
            System.out.println("Choose action:");
            System.out.println("1 - add user");
            System.out.println("2 - remove user");
            System.out.println("0 - stop");

            try {
                choice = scanner.nextInt();
                scanner.nextLine();
            }catch (InputMismatchException e) {
                System.out.println("Invalid value");
                choice = 0;
            }

            switch (choice) {
                case 1:
                    System.out.println("ADD USER ");
                    name = userRegistry.readName();
                    age = userRegistry.readAge();
                    userRegistry.addUser(users, name, age);
                    break;
                case 2:
                    System.out.println("REMOVE USER ");
                    name = userRegistry.readName();
                    userRegistry.removeUser(users, name);
                    break;
                default:
                    System.out.println("Stopping the app");
                    break;
            }
        } while (choice != 0);

        scanner.close();
        UserFileWriter.writeToFile(users, path);

    }

}
