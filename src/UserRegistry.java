import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by A on 02.08.2018.
 */
public class UserRegistry {

    private static Scanner scanner = new Scanner(System.in);

    public String readName(){
        System.out.println("Enter name: ");
        String name = "";
        try{
            name = scanner.nextLine();
        } catch (InputMismatchException e){
            System.out.println("Invalid value");
        }
        return name;
    }

    public Integer readAge(){
        System.out.println("Enter age: ");
        int age = 0;
        try{
            age = scanner.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Invalid value");
        }
        scanner.nextLine();
        return age;
    }

    public Map<String,Integer> addUser(Map<String,Integer> users, String name, int age){
        users.put(name,age);
        return users;
    }

    public Map<String,Integer> removeUser(Map<String,Integer> users, String name){
        users.remove(name);
        return users;
    }
}
