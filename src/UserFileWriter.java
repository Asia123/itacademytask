import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by A on 02.08.2018.
 */
public class UserFileWriter {


    public static void writeToFile(Map<String,Integer> users, String path){

         try (BufferedWriter br = new BufferedWriter(new FileWriter(path))){

             StringBuilder content = new StringBuilder();

             for (Map.Entry<String, Integer> entry : users.entrySet()) {
                 String key = entry.getKey();
                 content.append(key);
                 content.append(';');
                 Object value = entry.getValue();
                 content.append(value);
                 content.append('\n');
             }

             br.write(content.toString());

         } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
